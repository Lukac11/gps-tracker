package com.example.gpstracker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

public class ProfileCreator extends AppCompatActivity {
    RadioButton rb_male,rb_female,rb_age1,rb_age2,rb_age3,rb_history,rb_community,rb_foodie,rb_everything;
    Button btn_createProfile, btn_showUser, btn_saveUser;
    TextView tv_profile,tv_route;
    EditText et_firstName, et_lastName;
    AppDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_creator);
        db=AppDatabase.getInstance(this);
        rb_male=findViewById(R.id.radio_male);
        rb_female=findViewById(R.id.radio_female);
        rb_age1=findViewById(R.id.radio_age1);
        rb_age2=findViewById(R.id.radio_age2);
        rb_age3=findViewById(R.id.radio_age3);
        rb_history=findViewById(R.id.radio_history);
        rb_community=findViewById(R.id.radio_community);
        btn_createProfile=findViewById(R.id.btn_CreateProfile);
        tv_profile=findViewById(R.id.tv_profile);
        tv_route=findViewById(R.id.tv_route);
        rb_foodie=findViewById(R.id.radio_foodie);
        rb_everything=findViewById(R.id.radio_everything);
        et_firstName=findViewById(R.id.et_firstName);
        et_lastName=findViewById(R.id.et_lastName);
        btn_showUser=findViewById(R.id.btn_showUsers);
        btn_saveUser=findViewById(R.id.btn_saveUsers);
        btn_createProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rb_female.isChecked() && rb_age2.isChecked() && rb_community.isChecked()){
                    tv_profile.setText("You are: PARTYWOMAN");
                    tv_route.setText("You should check out: Fort Pub, Club Memories, Pivnice i sobe Merlon, Q club, Pivnica Bure bar");
                }
                if(rb_female.isChecked() && rb_age1.isChecked() && rb_community.isChecked()){
                    tv_profile.setText("You are: PARTYWOMAN");
                    tv_route.setText("You should check out: Fort Pub, Club Memories, Pivnice i sobe Merlon, Q club, Pivnica Bure bar");
                }
                if(rb_female.isChecked() && rb_age3.isChecked() && rb_community.isChecked()){
                    tv_profile.setText("You are: PARTYWOMAN");
                    tv_route.setText("You should check out: Fort Pub, Muzej Slavonije, Pivnice i sobe Merlon, Kružni pil Presvetog Trojstva, Pivnica Bure bar");
                }
                if(rb_female.isChecked() && rb_age1.isChecked() && rb_history.isChecked()){
                    tv_profile.setText("You are: HISTORIAN");
                    tv_route.setText("You should check out: Vodena Vrata Tvrđa, Muzej školjaka i vodenog svijeta, Crkva Sv. Križa, Muzej Slavonije, Crkva sv.Mihaela Arkanđela");
                }
                if(rb_female.isChecked() && rb_age2.isChecked() && rb_history.isChecked()){
                    tv_profile.setText("You are: HISTORIAN");
                    tv_route.setText("You should check out: Vodena Vrata Tvrđa, Arheološki muzej, Crkva Sv. Križa, Muzej Slavonije, Crkva sv.Mihaela Arkanđela");
                }
                if(rb_female.isChecked() && rb_age3.isChecked() && rb_history.isChecked()){
                    tv_profile.setText("You are: HISTORIAN");
                    tv_route.setText("You should check out: Vodena Vrata Tvrđa, Muzej školjaka i vodenog svijeta, Crkva Sv. Križa, Muzej Slavonije, Crkva sv.Mihaela Arkanđela");
                }
                if(rb_everything.isChecked()){
                    tv_profile.setText("You are: ALL ROUNDER");
                    tv_route.setText("You should check out: Vodena vrata Tvrđe, Club Memories, Kružni pil Presvetog Trojstva, Pivnice i sobe Merlon, Muzej Slavonije");
                }
                if(rb_foodie.isChecked()){
                    tv_profile.setText("You are: GOURMAND");
                    tv_route.setText("You should check out: Pivnice i sobe Merlon, Club Memories, Restoran Slavonska kuća, Petar Pan, Kružni pil Presvetog Trojstva");
                }
                if(rb_male.isChecked()&& rb_age2.isChecked() && rb_history.isChecked()){
                    tv_profile.setText("You are: HISTORIAN");
                    tv_route.setText("You should check out: Vodena Vrata Tvrđa, Muzej školjaka i vodenog svijeta, Fort Pub, Muzej Slavonije, Restoran Slavonska Kuća");
                }
                if(rb_male.isChecked() && rb_age3.isChecked() && rb_history.isChecked()){
                    tv_profile.setText("You are: HISTORIAN");
                    tv_route.setText("You should check out: Muzej školjaka i vodenog svijeta, Muzej Slavonije, Pivnica i sobe Merlon, Vodena vrata Tvrđa, Arheološki muzej Osijek");
                }
                if(rb_male.isChecked()&& rb_age1.isChecked() && rb_history.isChecked()){
                    tv_profile.setText("You are: HISTORIAN");
                    tv_route.setText("You should check out: Vodena Vrata Tvrđa, Muzej školjaka i vodenog svijeta, Fort Pub, Muzej Slavonije, Arheološki muzej");
                }
                if(rb_male.isChecked()&&rb_age1.isChecked()&&rb_community.isChecked()){
                    tv_profile.setText("You are: PARTYMAN");
                    tv_route.setText("You should check out: Club Memories, Q club, Pivnica Bure bar, Kružni pil Presvetog Trojstva, Aeroklub Osijek");
                }
                if(rb_male.isChecked()&&rb_age2.isChecked()&&rb_community.isChecked()){
                    tv_profile.setText("You are: PARTYMAN");
                    tv_route.setText("You should check out: Club Memories, Q club, Pivnica Bure bar, Kružni pil Presvetog Trojstva, Državni Arhiv");
                }
                if(rb_male.isChecked()&&rb_age3.isChecked()&&rb_community.isChecked()){
                    tv_profile.setText("You are: PARTYMAN");
                    tv_route.setText("You should check out: Pivnice i sobe Merlon, Fort pub, Pivnica Bure bar, Kružni pil Presvetog Trojstva, Državni Arhiv");
                }



            }
        });
        btn_saveUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.userDao().insertAll(new User(et_firstName.getText().toString(),et_lastName.getText().toString(),ages(),sex(),interest(),tv_profile.getText().toString(),tv_route.getText().toString()));
            }
        });
        btn_showUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ProfileCreator.this, UserList.class);
                startActivity(i);
            }
        });

    }
    public String ages(){
        if(rb_age1.isChecked()){
            return "18-35";
        }
        if(rb_age2.isChecked()){
            return "36-60";
        }
        if(rb_age3.isChecked()){
            return "60+";
        }
        return null;
    }
    public String sex(){
        if(rb_male.isChecked()){
            return "Male";
        }
        if(rb_female.isChecked()){
            return "Female";
        }
        return null;
    }
    public String interest(){
        if(rb_history.isChecked()){
            return "History";
        }
        if(rb_community.isChecked()){
            return "Community";
        }
        if(rb_everything.isChecked()){
            return "Everything";
        }
        if(rb_foodie.isChecked()){
            return "foodie";
        }
        return null;
    }



}