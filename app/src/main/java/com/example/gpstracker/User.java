package com.example.gpstracker;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
@Entity(tableName = "user")
public class User {
    @PrimaryKey(
            autoGenerate = true
    )
    public int uid;

    @ColumnInfo(name = "first_name")
    public String firstName;

    @ColumnInfo(name = "last_name")
    public String lastName;
    @ColumnInfo(name="Years")
    public String years;
    @ColumnInfo(name="Sex")
    public String sex;
    @ColumnInfo(name="Interest")
    public String interest;
    @ColumnInfo(name="Profile")
    public String profile;
    @ColumnInfo(name="Route")
    public String route;

    public User(String firstName, String lastName, String years, String sex, String interest, String profile, String route) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.years = years;
        this.sex = sex;
        this.interest = interest;
        this.profile = profile;
        this.route = route;
    }

    @Override
    public String toString() {
        return
                firstName + ' ' +
                 lastName + ' '+
                ", " + years +
                ", '" + sex +
                ", interest='" + interest +
                ", " + profile +
                ", route='" + route;


    }
}
