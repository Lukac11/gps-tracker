package com.example.gpstracker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class opening_activity extends AppCompatActivity {
    Button btn_zapocniObilazak, btn_kreirajProfil;
    TextView tv_naslov;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opening_activity);
        btn_zapocniObilazak=findViewById(R.id.btn_zapocniObilazak);
        btn_kreirajProfil=findViewById(R.id.btn_kreirajProfil);
        tv_naslov=findViewById(R.id.textView);
        btn_zapocniObilazak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(opening_activity.this,MainActivity.class);
                startActivity(i);
            }
        });
        btn_kreirajProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(opening_activity.this,ProfileCreator.class);
                startActivity(i);
            }
        });
    }
}