package com.example.gpstracker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    TextView tv_lat, tv_lon, tv_address, tv_pinCount, tv_popupText;
    Switch sw_locationupdates;
    Button btn_newPin, btn_showPinList,btn_showMap,btn_showMore;
    FusedLocationProviderClient fusedLocationProviderClient;
    RadioButton rb_historian, rb_partyman, rb_gourmand, rb_allrounder, rb_low, rb_medium, rb_high;
    ImageView iv_popup;
    boolean updateOn=false;
    LocationRequest locationRequest;
    LocationCallback locationCallBack;
    Location currentLocation;
    List<Location> savedLocations;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_lat=findViewById(R.id.tv_Latitude);
        tv_lon=findViewById(R.id.tv_Longitude);
        tv_address=findViewById(R.id.tv_address);
        sw_locationupdates=findViewById(R.id.sw_Update);
        btn_newPin =findViewById(R.id.btn_newPin);
        btn_showPinList =findViewById(R.id.btn_showPinList);
        tv_pinCount =findViewById(R.id.tv_savedLocations);
        btn_showMap=findViewById(R.id.btn_showMap);
        btn_showMore=findViewById(R.id.btn_showMore);
        iv_popup=findViewById(R.id.iv_slika);
        rb_historian=findViewById(R.id.radio_profile_historian);
        rb_partyman=findViewById(R.id.radio_profile_partyman);
        rb_gourmand=findViewById(R.id.radio_profile_gourmand);
        rb_allrounder=findViewById(R.id.radio_profile_everything);
        rb_low=findViewById(R.id.rb_low);
        rb_medium=findViewById(R.id.rb_medium);
        rb_high=findViewById(R.id.rb_high);

        locationRequest=new LocationRequest();
        locationRequest.setInterval(20000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if(rb_low.isChecked())
            locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        if(rb_high.isChecked()){
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        }
        locationCallBack=new LocationCallback(){

            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location location=locationResult.getLastLocation();
                if (location != null) {
                    updateLocationParameters(location);
                }

            }
        };

        sw_locationupdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sw_locationupdates.isChecked()){
                    startLocationUpdates();
                }else{
                    stopLocationUpdates();
                }
            }
        });
        btn_newPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication myApplication=(MyApplication)getApplicationContext();
                savedLocations=myApplication.getMyLocations();
                savedLocations.add(currentLocation);
            }
        });
        btn_showPinList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this,ShowSaveLocationsList.class);
                startActivity(i);
            }
        });
        btn_showMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this, MapsActivity.class);
                startActivity(i);
            }
        });
        btn_showMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater)
                        getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = inflater.inflate(R.layout.popup, null);
                int width = RelativeLayout.LayoutParams.WRAP_CONTENT;
                int height = RelativeLayout.LayoutParams.WRAP_CONTENT;
                boolean focus = true;
                final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focus);
                popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
                popupView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        popupWindow.dismiss();
                        return true;
                    }
            });
                if(distancebetweenLocations(currentLocation.getLatitude(),currentLocation.getLongitude(),45.56164,18.69831)<=0.05){
                    ((ImageView)popupWindow.getContentView().findViewById(R.id.iv_slika)).setImageResource(R.drawable.vodena_vrata);
                    if(rb_historian.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.vodenaVratahistory));
                    }
                    if(rb_gourmand.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.vodenaVratahistory));
                    }
                    if(rb_partyman.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.vodenaVrataParty));
                    }
                    if(rb_allrounder.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.vodenaVratahistory));
                    }
                }
                if(distancebetweenLocations(currentLocation.getLatitude(),currentLocation.getLongitude(),45.56104,18.69790)<=0.05){
                    ((ImageView)popupWindow.getContentView().findViewById(R.id.iv_slika)).setImageResource(R.drawable.skoljke);
                    if(rb_historian.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.muzejSkoljakaHistory));
                    }
                    if(rb_gourmand.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.muzejSkoljakaParty));
                    }
                    if(rb_partyman.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.muzejSkoljakaParty));
                    }
                    if(rb_allrounder.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.muzejSkoljakaParty));
                    }
                }
                if(distancebetweenLocations(currentLocation.getLatitude(),currentLocation.getLongitude(),45.56127,18.69727)<=0.05){
                    ((ImageView)popupWindow.getContentView().findViewById(R.id.iv_slika)).setImageResource(R.drawable.ante);
                    if(rb_historian.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.crkvaSvAnte));
                    }
                    if(rb_gourmand.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.crkvaSvAnte));
                    }
                    if(rb_partyman.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.crkvaSvAnte));
                    }
                    if(rb_allrounder.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.crkvaSvAnte));
                    }
                }
                if(distancebetweenLocations(currentLocation.getLatitude(),currentLocation.getLongitude(),45.56021,18.69647)<=0.05){
                    ((ImageView)popupWindow.getContentView().findViewById(R.id.iv_slika)).setImageResource(R.drawable.slavonije);
                    if(rb_historian.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.muzejSlavnoije));
                    }
                    if(rb_gourmand.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.muzejSlavonijekom));
                    }
                    if(rb_partyman.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.muzejSlavonijekom));
                    }
                    if(rb_allrounder.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.muzejSlavnoije));
                    }
                }
                if(distancebetweenLocations(currentLocation.getLatitude(),currentLocation.getLongitude(),45.56117,18.69422)<=0.05){
                    ((ImageView)popupWindow.getContentView().findViewById(R.id.iv_slika)).setImageResource(R.drawable.mihael);
                    ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.crkvaSvMihaela));
                }

                if(distancebetweenLocations(currentLocation.getLatitude(),currentLocation.getLongitude(),45.56054,18.69536)<=0.05){
                    ((ImageView)popupWindow.getContentView().findViewById(R.id.iv_slika)).setImageResource(R.drawable.arhmuzej);
                    if(rb_historian.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.ArhMuzejhist));
                    }
                    if(rb_gourmand.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.Arhmuzejall));
                    }
                    if(rb_partyman.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.ArhmuzejPart));
                    }
                    if(rb_allrounder.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.Arhmuzejall));
                    }
                }
                if(distancebetweenLocations(currentLocation.getLatitude(),currentLocation.getLongitude(),45.55963,18.69740)<=0.05){
                    ((ImageView)popupWindow.getContentView().findViewById(R.id.iv_slika)).setImageResource(R.drawable.buree);
                    ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.burebar));
                }
                if(distancebetweenLocations(currentLocation.getLatitude(),currentLocation.getLongitude(),45.56049,18.69577)<=0.05){
                    ((ImageView)popupWindow.getContentView().findViewById(R.id.iv_slika)).setImageResource(R.drawable.trg);
                    ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.trg));
                }
                if(distancebetweenLocations(currentLocation.getLatitude(),currentLocation.getLongitude(),45.55956,18.69469)<=0.05){
                    ((ImageView)popupWindow.getContentView().findViewById(R.id.iv_slika)).setImageResource(R.drawable.gimba);
                    ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.gimba));
                }
                if(distancebetweenLocations(currentLocation.getLatitude(),currentLocation.getLongitude(),45.55979,18.69903)<=0.05){
                    ((ImageView)popupWindow.getContentView().findViewById(R.id.iv_slika)).setImageResource(R.drawable.arhiv);
                    ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.arhiv));
                }
                //primjer
                if(distancebetweenLocations(currentLocation.getLatitude(),currentLocation.getLongitude(),45.310737,18.40585)<=0.05){
                    ((ImageView)popupWindow.getContentView().findViewById(R.id.iv_slika)).setImageResource(R.drawable.arhmuzej);
                    if(rb_historian.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.vodenaVratahistory));
                    }
                    if(rb_gourmand.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.vodenaVratahistory));
                    }
                    if(rb_partyman.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.vodenaVrataParty));
                    }
                    if(rb_allrounder.isChecked()){
                        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup_text)).setText(getResources().getString(R.string.vodenaVratahistory));
                    }
                }

            }
        });
        powerChanger();
        refreshLocation();
    }

    @SuppressLint("MissingPermission")
    private void stopLocationUpdates() {
        tv_lat.setText("##");
        tv_lon.setText("##");
        tv_address.setText("****");
        fusedLocationProviderClient.removeLocationUpdates(locationCallBack);
        new AlertDialog.Builder(this).setTitle("Location is not being tracked").setMessage("Do you want " +
                "to turn Location tracking back on?").setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sw_locationupdates.setChecked(true);
                refreshLocation();
            }
        }).setNegativeButton("NO",null).show();
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        fusedLocationProviderClient.requestLocationUpdates(locationRequest,locationCallBack, null);
        refreshLocation();
        Toast.makeText(this,"Location is being tracked",Toast.LENGTH_LONG);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==1){
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    refreshLocation();
                }else{
                    finish();
                }
        }
    }

    private void refreshLocation(){
        fusedLocationProviderClient= LocationServices.getFusedLocationProviderClient(this);
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if(location!=null) {
                        updateLocationParameters(location);
                        currentLocation = location;
                    }
                }
            });
        }else {
            if(Build.VERSION.SDK_INT>=23){
                requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }
    private void updateLocationParameters(Location location){
        tv_lat.setText(String.valueOf(location.getLatitude()));
        tv_lon.setText(String.valueOf(location.getLongitude()));

        Geocoder geocoder=new Geocoder(this);
        try{
            List<Address> addresses=geocoder.getFromLocation(location.getLatitude(),location.getLongitude(),1);
            tv_address.setText(addresses.get(0).getAddressLine(0));
        }
        catch(Exception e){
            tv_address.setText("Unable to get street address");
        }
        MyApplication myApplication=(MyApplication)getApplicationContext();
        savedLocations=myApplication.getMyLocations();
        tv_pinCount.setText(Integer.toString(savedLocations.size()));

    }
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.sideBtn) {
            Intent i=new Intent(MainActivity.this,ProfileCreator.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
    private double distancebetweenLocations(double lat1, double lng1, double lat2, double lng2) {

        double earthRadius = 6371;

        double degLat = Math.toRadians(lat2-lat1);
        double degLong = Math.toRadians(lng2-lng1);
        double sindegLat = Math.sin(degLat / 2);
        double sindegLng = Math.sin(degLong / 2);
        double a = Math.pow(sindegLat, 2) + Math.pow(sindegLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = earthRadius * c;
        return distance;
    }
    private void powerChanger(){
        if(rb_low.isChecked()){
            locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);

        }
        if(rb_medium.isChecked()){
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        }
        if(rb_high.isChecked()){
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        }

    }


}