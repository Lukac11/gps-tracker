package com.example.gpstracker;

import androidx.appcompat.app.AppCompatActivity;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ShowSaveLocationsList extends AppCompatActivity {
    ListView lv_savedLocations;
    String TAG="ShowSaveLocationsList";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_save_locations_list);
        MyApplication myApplication=(MyApplication)getApplicationContext();
        Geocoder geocoder=new Geocoder(this);
        List<Location> savedLocations=myApplication.getMyLocations();
        lv_savedLocations = findViewById(R.id.lv_wayPoints);
        List<Address> addresses=new ArrayList<Address>();
        List<String> trueAddresses=new ArrayList<String>();
        for (int i=0;i<savedLocations.size();i++) {
            try {
                trueAddresses.add(geocoder.getFromLocation(savedLocations.get(i).getLatitude(), savedLocations.get(i).getLongitude(), 1).get(0).getAddressLine(0));
            } catch (Exception e) {
                Log.e("Exception", "Unable to get address");
            }
        }
        lv_savedLocations.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,trueAddresses));
        Log.d(TAG, "onCreate: ");

    }


}