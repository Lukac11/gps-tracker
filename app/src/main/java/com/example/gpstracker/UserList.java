package com.example.gpstracker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class UserList extends AppCompatActivity {
    ListView lv_users;
    AppDatabase db=AppDatabase.getInstance(this);
    UserDao userDao=db.userDao();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        lv_users=findViewById(R.id.lv_userList);
        List<User> users = userDao.getAll();
        List<String> realUsers=new ArrayList<>();
        for(int i=0;i<users.size();i++){
            realUsers.add(i,users.get(i).toString());
        }
        lv_users.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,realUsers));
    }
}